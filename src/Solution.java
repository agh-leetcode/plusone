public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        int arr[] = new int[]{1,2,3};
        int arr2[] = new int[]{1,9,9,9};
        int arr3[] = new int[]{5,6,2,0,0,4,6,2,4,9};
        int arr4[] = new int[]{9,9};
        int[] result = plusOne(arr4);
        for (int i : result) {
            System.out.println(i);
        }
    }

    private int[] plusOne(int[] arr) {

        for (int i = arr.length - 1; i >= 0; --i) {
            if (arr[i]!=9){
                arr[i]++;
                return arr;
            }else {
                arr[i] = 0;
            }
        }
        int[] result = new int[arr.length + 1];
        result[0] = 1;
        System.arraycopy(arr, 0, result, 1, arr.length);

        return result;
    }
}
